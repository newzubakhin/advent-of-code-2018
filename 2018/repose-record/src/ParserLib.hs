module ParserLib
    ( parseGuardLog
    , parseAction
    , parseGuardNum
    , Action(..)
    )
where

import           Data.Time
import           Text.Parsec.Char
import           Text.ParserCombinators.Parsec
import           TimeLib

data Action = WakeUp | FallsAsleep | BeginsShift deriving (Show)

nat :: Parser Int
nat = read <$> many1 digit

dateTime :: Parser UTCTime
dateTime = do
    ds <- between (char '[') (char ']') (many $ noneOf "]")
    let d = parseDateTime ds
    case d of
        Just t -> return t
        _      -> unexpected $ show ds

action :: Parser Action
action =
    (string "falls asleep" >> return FallsAsleep)
        <|> (string "wakes up" >> return WakeUp)
        <|> (string "bigins shift" >> return BeginsShift)

parseAction = parse action "(unknown)"

parseGuardNum = parse guardNum "(unknown)"

guardNum =
    do
        string "Guard"
        skipMany space
        char '#'
        nat

guard =
    do
        dt <- dateTime
        skipMany1 space
        num <- guardNum
        skipMany1 space
        a <- action
        endOfLine
        al <- many action
        return $ a:al

line = do
    dt <- dateTime
    many space
    guard <- many $ noneOf "\n"
    return (dt, guard)

guardLog = do
    result <- endBy line endOfLine
    eof
    return result

-- guardLog = do
--     result <- many1 guard
--     eof
--     return result


parseGuardLog = parse guardLog "(unknown)"
