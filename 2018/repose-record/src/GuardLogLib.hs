module GuardLogLib
    ( processLog
    , getGuardSchedule
    , findMostAsleepGuard
    , findMostFrequently
    , pairMax
    )
where

import           ParserLib
import           LogLib
import           TimeLib
import           Data.Either
import qualified Data.Map                      as Map
import           Data.Set
import           Data.Time
import           Data.Time.Clock
import           Data.Time.Format
import           Data.List                      ( sortBy
                                                , isPrefixOf
                                                )
import           Data.Function                  ( on )
import           Control.Monad.State

type GuardNum = Int
type Minute = Int
type GuardNumState = State GuardNum
type GuardState = State (UTCTime, GuardNum, Action)


frequency :: Ord a => [a] -> Map.Map a Int
frequency input = Map.fromListWith (+) [ (c, 1) | c <- input ]

processLog
    :: [(UTCTime, String)]
    -> GuardNumState (Either String [(UTCTime, GuardNum, Action)])
processLog []                     = return $ Right []
processLog ((datetime, log) : xs) = if "Guard" `isPrefixOf` log
    then case parseGuardNum log of
        Left  e -> return $ Left $ show e
        Right r -> do
            put r
            let y = Right (datetime, r, BeginsShift)
            ys <- processLog xs
            return $ (:) <$> y <*> ys
    else case parseAction log of
        Left  e -> return $ Left $ show e
        Right r -> do
            guardNum <- get
            ys       <- processLog xs
            let y = Right (datetime, guardNum, r)
            return $ (:) <$> y <*> ys


getGuardSchedule
    :: [(UTCTime, Int, Action)]
    -> Map.Map GuardNum (Map.Map Minute Int)
    -> GuardState (Map.Map GuardNum (Map.Map Minute Int))
getGuardSchedule [] m = return m
getGuardSchedule ((datetime, guardNum, action) : xs) m = case action of
    FallsAsleep -> do
        put (datetime, guardNum, action)
        getGuardSchedule xs m
    WakeUp -> do
        (prevDatetime, prevGuardNum, prevAction) <- get
        let seq  = minutesSeq prevDatetime datetime
        let freq = frequency seq
        getGuardSchedule xs (Map.insertWith (Map.unionWith (+)) guardNum freq m)

    _ -> getGuardSchedule xs m

pairMax :: Ord a => [(t, a)] -> (t, a)
pairMax []       = error "Empty list"
pairMax (x : xs) = maxTail x xs
  where
    maxTail currentMax [] = currentMax
    maxTail (k, v) (p : ps) | v < snd p = maxTail p ps
                            | otherwise = maxTail (k, v) ps

sumSnd x = sum $ Prelude.map snd x

findMostAsleepGuard
    :: [(GuardNum, Map.Map Minute Int)] -> (GuardNum, Map.Map Minute Int)
findMostAsleepGuard []       = error "Empty list"
findMostAsleepGuard (x : xs) = maxTail x xs
  where
    maxTail currentMax [] = currentMax
    maxTail (k, v) (p : ps)
        | sumSnd (Map.toList v) < sumSnd (Map.toList (snd p)) = maxTail p ps
        | otherwise = maxTail (k, v) ps

findMostFrequently
    :: [(GuardNum, Map.Map Minute Int)] -> (GuardNum, Map.Map Minute Int)
findMostFrequently []       = error "Empty list"
findMostFrequently (x : xs) = maxTail x xs
  where
    maxTail currentMax [] = currentMax
    maxTail (k, v) (p : ps)
        | snd (pairMax (Map.toList v)) < snd (pairMax (Map.toList (snd p))) = maxTail
            p
            ps
        | otherwise = maxTail (k, v) ps
