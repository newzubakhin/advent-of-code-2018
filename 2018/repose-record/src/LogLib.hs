module LogLib
    (sortLog)
where

import           Data.List                      ( sortBy )
import           Data.Function                  ( on )

sortLog :: Ord a => [(a, x)] -> [(a, x)]
sortLog = sortBy (compare `on` fst)
