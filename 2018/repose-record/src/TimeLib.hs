module TimeLib
    ( parseDateTime
    , minutesSeq
    , timeSeq
    , getMinute
    )
where
import           Data.Time
import           Data.Time.Format
import           Data.Time.Clock

parseDateTime :: String -> Maybe UTCTime
parseDateTime = parseTimeM True defaultTimeLocale "%Y-%m-%d %H:%M"

minutesSeq :: UTCTime -> UTCTime -> [Int]
minutesSeq start end = map getMinute (timeSeq start end 60)

timeSeq :: UTCTime -> UTCTime -> NominalDiffTime -> [UTCTime]
timeSeq start end step = takeWhile (< end) $ iterate (addUTCTime step) start

getMinute :: UTCTime -> Int
getMinute time = todMin $ timeToTimeOfDay $ utctDayTime time
