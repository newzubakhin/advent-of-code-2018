module Main where

import           ParserLib
import           GuardLogLib
import           LogLib
import qualified Data.Map                      as Map
import           Control.Monad.State


main :: IO ()
main = do
    c <- getContents
    case parseGuardLog c of
        Left e -> do
            putStrLn "Error parsing"
            print e
        Right r -> case evalState (processLog (sortLog r)) 0 of
            Left  e -> print e
            Right r -> do
                let startState      = Prelude.head r
                let log = evalState (getGuardSchedule r Map.empty) startState
                let mostAsleepGuard = findMostAsleepGuard $ Map.toList log
                let mostFreq        = findMostFrequently $ Map.toList log
                print $ fst mostAsleepGuard * fst
                    (pairMax (Map.toList $ snd mostAsleepGuard))
                print $ fst mostFreq * fst (pairMax (Map.toList $ snd mostFreq))
