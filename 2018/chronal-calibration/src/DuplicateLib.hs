module DuplicateLib
    ( findDup
    )
where

import qualified Data.Set                      as Set

dup :: Ord a => [a] -> Maybe a
dup xs = dup' xs Set.empty
  where
    dup' :: Ord a => [a] -> Set.Set a -> Maybe a
    dup' [] _ = Nothing
    dup' (x : xs) s =
        if Set.member x s then Just x else dup' xs (Set.insert x s)

findDup :: (Ord a, Show a) => [a] -> String
findDup x = case dup x of
    Just x  -> show x
    Nothing -> "No duplicates"
