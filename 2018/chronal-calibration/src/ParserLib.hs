module ParserLib
    ( parseSequence
    )
where

import           Text.ParserCombinators.Parsec

opSymbol = oneOf "-+"

nat :: Parser Int
nat = read <$> many1 digit

sign :: Parser Int
sign = do
    sign <- opSymbol
    return $ case sign of
        '-' -> -1
        _   -> 1

frequencySequence =
    do
        result <- endBy line eol
        eof
        return result

eol =   try (string "\n\r")
    <|> try (string "\r\n")
    <|> string "\n"
    <|> string "\r"
    <?> "end of line"

line :: Parser Int
line = do
    numSign <- sign
    num     <- nat
    return $ numSign * num

parseSequence = parse frequencySequence "(unknown)"
