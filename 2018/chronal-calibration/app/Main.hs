module Main where

import           ParserLib
import           DuplicateLib

main :: IO ()
main = do
    c <- getContents
    case parseSequence c of
        Left e -> do
            putStrLn "Error parsing:"
            print e
        Right r -> print $ findDup $ scanl (+) 0 $ cycle r
