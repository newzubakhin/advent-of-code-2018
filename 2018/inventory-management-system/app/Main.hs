module Main where

import           Lib
import qualified Data.Map                      as Map

main :: IO ()
main = do
    c <- getContents
    let frequency = map letterFrequency (lines c)
    let twoLetter =
            length $ filter (not . null) $ map (Map.filter (== 3)) frequency
    let threeLetter =
            length $ filter (not . null) $ map (Map.filter (== 2)) frequency
    print $ twoLetter * threeLetter
    print $ maximum $ map findTrueBoxes (combinaitons (lines c))
