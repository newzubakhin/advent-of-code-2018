module Lib
    ( letterFrequency
    , combinaitons
    , findTrueBoxes
    ) where
import qualified Data.Map                      as Map

letterFrequency :: String -> Map.Map Char Int
letterFrequency input = Map.fromListWith (+) [ (c, 1) | c <- input ]

combinaitons :: [a] -> [(a,a)]
combinaitons []       = []
combinaitons (x : xs) = map ((,) x) xs ++ combinaitons xs

diff :: Eq a => ([a], [a]) -> [a]
diff ([]    , _     ) = []
diff (_     , []    ) = []
diff (x : xs, y : ys) = if x == y then x : diff (xs, ys) else diff (xs, ys)

findTrueBoxes :: Eq a => ([a], [a]) -> Maybe [a]
findTrueBoxes (x, y) = if length x - 1 == length diff' then Just diff' else Nothing
    where diff' = diff (x, y)
