module Main where

import           Lib

import           Data.Char

react :: Char -> Char -> Bool
x `react` y = (x == toUpper y || toUpper x == y) && x /= y

reduction :: String -> String -> String
reduction [] [] = []
reduction [] x = x
reduction (x:xs) [] = reduction xs [x]
reduction (x:xs) (y:ys) = if x `react` y then reduction xs ys else reduction xs (x:y:ys)

main :: IO ()
main = do
    c <- getContents
    print $ length $ reduction (filter (/='\n') c) []
